<?php

namespace App\Http\Controllers;

use App\marcas;
use App\Http\Requests\reqMarcas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarcasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'marcas';
    public function index()
    {
        $p = DB::table("marcas")
            ->select("marcas.*")
            ->paginate(10);
        return view($this->table.'.index',[
            'table' => $this->table,
            'title' => 'Listado de Marcas',
            'data' => $p
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger','Debe llenar el campo para efectuar una busqueda');

        return view($this->table.'.index', [
            'table'     => $this->table,
            'title'     => 'Listado de Marcas',
            'data'      => marcas::where('marca','like','%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', [
            'table' => $this->table, 
            'title'=>'Agregar Marcas'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqMarcas $request)
    {
        $marca = new marcas();
        $marca->marca = $request->marcas;
        $e = $marca->save();
        return redirect()->route($this->table.'.index')->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar la marca intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\marcas  $marcas
     * @return \Illuminate\Http\Response
     */
    public function show(marcas $marcas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\marcas  $marcas
     * @return \Illuminate\Http\Response
     */
    public function edit(marcas $marcas, $marca)
    {
        $data = $marcas::findOrFail($marca);
        return view($this->table.'.edit', [
            'table' => $this->table,
            'title' => 'Editar Marcas',
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\marcas  $marcas
     * @return \Illuminate\Http\Response
     */
    public function update(reqMarcas $request, marcas $col, $marcas)
    {
        $data = $col::findOrFail($marcas);
        $data->marca = $request->marcas;
        $e = $data->save();
        return redirect()->route($this->table.'.index')->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el color intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\marcas  $marcas
     * @return \Illuminate\Http\Response
     */
    public function destroy(marcas $marcas, $data)
    {
        $obj = $marcas::findOrFail($data);
        $m = $obj->delete();
        return redirect()->route($this->table.'.index')->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
