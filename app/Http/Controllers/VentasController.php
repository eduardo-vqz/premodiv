<?php

namespace App\Http\Controllers;

use App\ventas;
use App\User;
use App\Http\Requests\reqVentas;
use App\Http\Requests\reqVentasEdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'ventas';
    public function index()
    {
        $p = DB::table('users')
        ->join('ventas', function ($join) {
            $join->on('users.id', '=', 'ventas.user_id');
        })
        ->paginate(10);

        return view($this->table.'.index',[
            'table' => $this->table,
            'title' => 'Listado de Ventas',
            'data' => $p
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger','Debe llenar el campo para efectuar una busqueda');

        return view($this->table.'.index', [
            'table'     => $this->table,
            'title'     => 'Listado de ventas',
            'data'      => ventas::where('created_at','like','%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $p = DB::table('users')
        ->join('ventas', function ($join) {
            $join->on('users.id', '=', 'ventas.user_id');
            })
            ->get();
        return view($this->table.'.create',[
            'table' => $this->table,
			'title' => 'agregar ventas',
            'ventas' => ventas::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqVentas $request)
    {
        $ventas     = new ventas();
        $ventas->user_id    = auth()->id();
        $ventas->numero_factura =$request->numero_factura;
        $ventas->cliente        =$request->cliente;

        $e = $ventas->save();

        return redirect()->route($this->table . '.index')->with(($e) ?
			'info' : 'danger', ($e) ?
			'Guardado con exito' : 'Ocurrio un problema al guardar la venta intente de nuevo.');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \App\ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function show(ventas $ventas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function edit(ventas $ventas, $venta)
    {
        $data = $ventas::findOrFail($venta);
		return view($this->table . '.edit', [
			'table'        => $this->table,
			'title'        => 'Editar venta',
			'data'         => $data,
			'users'       => User::all()

		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function update(reqVentasEdit $request, ventas $col, $ventas)
    {
        $data = $col::findOrFail($ventas);
		$data->numero_factura       = $request->numero_factura;
		$data->cliente              = $request->cliente;
		$e                          = $data->save();
		return redirect()->route($this->table . '.index')->with(($e) ? 'info' : 'danger', ($e) ? 'Se edito un registro con exito. ' : 'Ocurrio un problema al editar el color intente de nuevo.'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ventas  $ventas
     * @return \Illuminate\Http\Response
     */
    public function destroy(ventas $ventas)
    {
        //
    }
}
