<?php

namespace App\Http\Controllers;

use App\carros;
use App\colores;
use App\estado_carros;
use App\fotos_carros;
use App\marcas;
use App\Http\Requests\reqCarros;
use App\Http\Requests\reqCarrosEdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Foreach_;

class CarrosController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	private $table = 'carros';
	public function index()
	{
		$foto_car = fotos_carros::all();
		$p = DB::table("carros")
			->join('colores', 'carros.color_id', '=', 'colores.id')
			->join('modelos', 'carros.modelo_id', '=', 'modelos.id')
			->join('marcas', 'modelos.marca_id', '=', 'marcas.id')
			->join('estado_carros', 'carros.estado_carro_id', '=', 'estado_carros.id')
			->select('carros.*', 'marcas.marca', 'colores.colores', 'modelos.modelo', 'estado_carros.estado_carro')
			->paginate(10);

		return view($this->table . '.index', [
			'table' => $this->table,
			'title' => 'Listado de carros',
			'data'  => $p,
			'foto'  => $foto_car
		]);
	}

	public function search(Request $r)
	{
		if (!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
			return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');
		$p = DB::table("carros")
			->join('colores', 'carros.color_id', '=', 'colores.id')
			->join('modelos', 'carros.modelo_id', '=', 'modelos.id')
			->join('marcas', 'modelos.marca_id', '=', 'marcas.id')
			->join('estado_carros', 'carros.estado_carro_id', '=', 'estado_carros.id')
			->select('carros.*', 'marcas.marca', 'colores.colores', 'modelos.modelo', 'estado_carros.estado_carro')
			->where('marcas.marca', 'like', '%' . $r->txtSearch . '%')->paginate();
		
			return view($this->table . '.index', [
			'table'     =>  $this->table,
			'title'     => 'Listado de Colores',
			'data'      => $p,
			'txtSearch' => $r->txtSearch,
		]);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$p = DB::table("modelos")
			->join("marcas", "modelos.marca_id", "=", "marcas.id")
			->select("modelos.*", "marcas.marca")
			->get();
		return view($this->table . '.create', [
			'table'          =>  $this->table,
			'title'          => 'Agregar carro',
			'modelos_marcas' => $p,
			'estado_carros'  => estado_carros::all(),
			'colores'        => colores::all()
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(reqCarros $request)
	{

		$carro                  = new carros();
		$carro->color_id        = $request->color_id;
		$carro->modelo_id       = $request->modelo_id;
		$carro->annio           = $request->annio;
		$carro->detalles        = $request->detalles;
		$carro->estado_carro_id = $request->estado_carro_id;
		$carro->precio          = $request->precio;

		$e  = $carro->save();

		if ($request->hasFile('foto_carro')) {

			$file = $request->file('foto_carro');

			foreach ($file as $key => $value) {
				$foto_carro = new fotos_carros();
				$name       = time() . $key . str_replace(" ", "", $value->getClientOriginalName());

				$value->move(public_path() . '/img/', $name);
				/* echo $value . " - " . $name . "<br>"; */
				$foto_carro->carro_id   = $carro->id;
				$foto_carro->foto_carro = $name;
				$foto_carro->save();
			}
		}
		return redirect()->route($this->table . '.index')->with(($e) ?
			'info' : 'danger', ($e) ?
			'Guardado con exito' : 'Ocurrio un problema al guardar el carro intente de nuevo.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\carros  $carros
	 * @return \Illuminate\Http\Response
	 */
	public function show(carros $carros)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\carros  $carros
	 * @return \Illuminate\Http\Response
	 */
	public function edit(carros $carros, $carro)
	{
		$data = $carros::findOrFail($carro);
		$p = DB::table("modelos")
			->join("marcas", "modelos.marca_id", "=", "marcas.id")
			->select("modelos.*", "marcas.marca")
			->get();

		return view($this->table . '.edit', [
			'table'          =>  $this->table,
			'title'          => 'Agregar carro',
			'modelos_marcas' => $p,
			'estado_carros'  => estado_carros::all(),
			'colores'        => colores::all(),
			'data'           => $data
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\carros  $carros
	 * @return \Illuminate\Http\Response
	 */
	public function update(reqCarrosEdit $request, carros $col, $carros)
	{
		$data = $col::findOrFail($carros);

		if ($request->hasFile('foto_carro')) {
			// Eliminar imagenes de la carpeta public/img
			// Eliminar imagenes de la db para reemplazarlas
			$f_carro = new fotos_carros();
			foreach ($f_carro::all() as $key => $value) {
				if ($value->carro_id == $data->id) {
					//echo $value->foto_carro . "<br>";
					$image_path = public_path() . "/img/" . $value->foto_carro;
					unlink($image_path);
					$value->delete();
				}
			}

			//Insercion de nuevas imagenes
			$file = $request->file('foto_carro');
			foreach ($file as $key => $value) {
				$foto_carro = new fotos_carros();
				$name       = time() . $key . str_replace(" ", "", $value->getClientOriginalName());

				$value->move(public_path() . '/img/', $name); 
				//echo $value . " - " . $name . "<br>"; 
		 		$foto_carro->carro_id   = $data->id;
				$foto_carro->foto_carro = $name;
				$foto_carro->save();
			}  
		}

		//Actualización de datos de la tabla carros
		$data->modelo_id       = $request->modelo_id;
		$data->color_id        = $request->color_id;
		$data->annio           = $request->annio;
		$data->detalles        = $request->detalles;
		$data->estado_carro_id = $request->estado_carro_id;
		$data->precio          = $request->precio;
		$e                     = $data->save();

		return redirect()->route($this->table . '.index')->with(($e) ? 
		'info' : 'danger', ($e) ? 
		'Se edito un registro con exito. ' : 'Ocurrio un problema al editar el carro intente de nuevo.'); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\carros  $carros
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(carros $carros)
	{
		//
	}
}
