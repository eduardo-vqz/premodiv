<?php

namespace App\Http\Controllers;

use App\Http\Requests\reqModelos;
use App\Http\Requests\reqModelosEdit;
use App\marcas;
use App\modelos;
use App\tipos_carros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ModelosController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	private $table = "modelos";
	public function index()
	{
		$p = DB::table("modelos")
			->join("tipos_carros", "modelos.tipo_carro_id", "=", "tipos_carros.id")
			->join("marcas", "modelos.marca_id", "=", "marcas.id")
			->select("modelos.*", "tipos_carros.tipo_carro", "marcas.marca")
			->paginate(10);
		//var_dump($p);
		return view($this->table . '.index', [
			'table' =>  $this->table,
			'title' => 'Listado de modelos',
			'data'  => $p
		]);
	}

	public function search(Request $r)
	{
		if (!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
			return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

		$p = DB::table("modelos")
			->join("tipos_carros", "modelos.tipo_carro_id", "=", "tipos_carros.id")
			->join("marcas", "modelos.marca_id", "=", "marcas.id")
			->select("modelos.*", "tipos_carros.tipo_carro", "marcas.marca")
			->where('modelo', 'like', '%' . $r->txtSearch . '%')
			->paginate();

		return view($this->table . '.index', [
			'table'     =>  $this->table,
			'title'     => 'Listado de modelos',
			'data'      => $p,
			'txtSearch' => $r->txtSearch,
		]);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view($this->table . '.create', [
			'table' => $this->table,
			'title' => 'agregar modelo',
			'marcas' => marcas::all(),
			'tipos_carros' => tipos_carros::all(),
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(reqModelos $request)
	{
		$modelo                = new modelos();
		$modelo->modelo        = $request->modelo;
		$modelo->tipo_carro_id = $request->tipo_carro_id;
		$modelo->marca_id      = $request->marca_id;
		$e  = $modelo->save();
		return redirect()->route($this->table . '.index')->with(($e) ?
			'info' : 'danger', ($e) ?
			'Guardado con exito' : 'Ocurrio un problema al guardar el color intente de nuevo.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\modelos  $modelos
	 * @return \Illuminate\Http\Response
	 */
	public function show(modelos $modelos)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\modelos  $modelos
	 * @return \Illuminate\Http\Response
	 */
	public function edit(modelos $modelos, $modelo)
	{
		//

		$data = $modelos::findOrFail($modelo);
		return view($this->table . '.edit', [
			'table'        => $this->table,
			'title'        => 'Editar modelos',
			'data'         => $data,
			'marcas'       => marcas::all(),
			'tipos_carros' => tipos_carros::all()

		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\modelos  $modelos
	 * @return \Illuminate\Http\Response
	 */
	public function update(reqModelosEdit $request, modelos $col, $modelos)
	{
		$data = $col::findOrFail($modelos);
		$data->modelo        = $request->modelo;
		$data->tipo_carro_id = $request->tipo_carro_id;
		$data->marca_id      = $request->marca_id;
		$e                   = $data->save();
		return redirect()->route($this->table . '.index')->with(($e) ? 'info' : 'danger', ($e) ? 'Se edito un registro con exito. ' : 'Ocurrio un problema al editar el color intente de nuevo.'); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\modelos  $modelos
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(modelos $modelos, $modelo)
	{
		//
	}
}
