<?php

namespace App\Http\Controllers;

use App\detalle_ventas;
use App\ventas;
use App\carros;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DetalleVentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = "detalle_ventas";
    public function index()
    {
        $p = DB::table('ventas')
            ->join('detalle_ventas', function ($join) {
                $join->on('ventas.id', '=', 'detalle_ventas.venta_id');
            })
            ->paginate(10);
		//var_dump($p);
		return view($this->table . '.index', [
			'table' =>  $this->table,
			'title' => 'Listado de modelos',
			'data'  => $p
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table . '.create', [
			'table' => $this->table,
			'title' => 'agregar detalles de venta',
			'ventas' => ventas::all(),
			'carros' => carros::all(),
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\detalle_ventas  $detalle_ventas
     * @return \Illuminate\Http\Response
     */
    public function show(detalle_ventas $detalle_ventas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\detalle_ventas  $detalle_ventas
     * @return \Illuminate\Http\Response
     */
    public function edit(detalle_ventas $detalle_ventas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\detalle_ventas  $detalle_ventas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, detalle_ventas $detalle_ventas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\detalle_ventas  $detalle_ventas
     * @return \Illuminate\Http\Response
     */
    public function destroy(detalle_ventas $detalle_ventas)
    {
        //
    }
}
