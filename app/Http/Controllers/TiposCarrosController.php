<?php

namespace App\Http\Controllers;

use App\tipos_carros;
use Illuminate\Http\Request;
use App\Http\Requests\reqTiposCarros;
use App\Http\Requests\reqTiposCarrosEdit;
use Illuminate\Support\Facades\DB;

class TiposCarrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'tipos_carros';
    public function index()
    {
        $p = DB::table("tipos_carros")
            ->select("tipos_carros.*")
            ->paginate(10);
        return view($this->table.'.index',[
            'table' => $this->table,
            'title' => 'Listado de tipos_carros',
            'data' => $p
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de tipos de carros',
            'data'      => tipos_carros::where('tipo_carro', 'like', '%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', ['table' =>  $this->table, 'title'=>'Agregar Tipos de carros']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqTiposCarros $request)
    {
        $tipos_carros = new tipos_carros();
        $tipos_carros->tipo_carro = $request->tipos_carros;
        $e  = $tipos_carros->save();
        return redirect()->route($this->table.'.index')->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar el color intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipos_carros  $tipos_carros
     * @return \Illuminate\Http\Response
     */
    public function show(tipos_carros $tipos_carros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipos_carros  $tipos_carros
     * @return \Illuminate\Http\Response
     */
    public function edit(tipos_carros $tipos_carros, $tipo_carro)
    {
        $data = $tipos_carros::findOrFail($tipo_carro);
        return view($this->table.'.edit', [
            'table' =>  $this->table,
            'title'=>'Editar tipos de carros',
            'data'=> $data
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipos_carros  $tipos_carros
     * @return \Illuminate\Http\Response
     */
    public function update(reqTiposCarrosEdit $request, tipos_carros $col, $tipos_carros)
    {
        $data = $col::findOrFail($tipos_carros);
        $data->tipo_carro = $request->tipos_carros;
        $e = $data->save();
        return redirect()->route($this->table.'.index')->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el color intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipos_carros  $tipos_carros
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipos_carros $tipos_carros, $data)
    {
        $obj = $tipos_carros::findOrFail($data);
        $m = $obj->delete();
        return redirect()->route($this->table.'.index')->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
