<?php

namespace App\Http\Controllers;

use App\colores;
use App\Http\Requests\reqColores;
use App\Http\Requests\reqColoresEdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ColoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //TODO ALGO
    private $table = 'colores';
    public function index()
    {
        $p = DB::table("colores")
            ->select("colores.*")
            ->paginate(10);
        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Listado de Colores',
            'data'  => $p
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     =>'Listado de Colores',
            'data'      => colores::where('colores', 'like', '%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table.'.create', [
            'table' => $this->table, 
            'title' => 'Agregar Colores'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqColores $request)
    {
        $color = new colores();
        $color->colores = $request->colores;
        $e  = $color->save();
        return redirect()->route($this->table.'.index')->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar el color intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\colores  $colores
     * @return \Illuminate\Http\Response
     */
    public function show(colores $colores)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\colores  $colores
     * @return \Illuminate\Http\Response
     */
    public function edit(colores $colores,$color)
    {
        $data = $colores::findOrFail($color);
        return view($this->table.'.edit', [
            'table' =>  $this->table,
            'title'=>'Editar Colores',
            'data'=> $data
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\colores  $colores
     * @return \Illuminate\Http\Response
     */
    public function update(reqColoresEdit $request, colores $col, $colores)
    {
        $data = $col::findOrFail($colores);
        $data->colores = $request->colores;
        $e = $data->save();
        return redirect()
        ->route($this->table.'.index')
        ->with(($e)?
        'info':'danger',($e)?
        'Se edito un registro con exito. ':'Ocurrio un problema al editar el color intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\colores  $colores
     * @return \Illuminate\Http\Response
     */
    public function destroy(colores $colores,$data)
    {
        $obj = $colores::findOrFail($data);
        $m = $obj->delete();
        return redirect()->route($this->table.'.index')->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
 