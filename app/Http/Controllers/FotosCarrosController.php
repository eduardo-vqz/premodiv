<?php

namespace App\Http\Controllers;

use App\fotos_carros;
use Illuminate\Http\Request;

class FotosCarrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\fotos_carros  $fotos_carros
     * @return \Illuminate\Http\Response
     */
    public function show(fotos_carros $fotos_carros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\fotos_carros  $fotos_carros
     * @return \Illuminate\Http\Response
     */
    public function edit(fotos_carros $fotos_carros)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\fotos_carros  $fotos_carros
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, fotos_carros $fotos_carros)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\fotos_carros  $fotos_carros
     * @return \Illuminate\Http\Response
     */
    public function destroy(fotos_carros $fotos_carros)
    {
        //
    }
}
