<?php

namespace App\Http\Controllers;

use App\solicitudes;
use App\User;
use App\Http\Requests\reqSolicitudes;
use App\Http\Requests\reqSolicitudesEdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SolicitudesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'solicitudes';
    public function index()
    {
        $p = DB::table('users')
        ->join('solicitudes', function ($join) {
            $join->on('users.id', '=', 'solicitudes.user_id');
        })
        ->paginate(10);

        return view($this->table.'.index',[
            'table' => $this->table,
            'title' => 'Listado de Solicitudes',
            'data' => $p
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger','Debe llenar el campo para efectuar una busqueda');

        return view($this->table.'.index', [
            'table'     => $this->table,
            'title'     => 'Listado de Solicitudes',
            'data'      => solicitudes::where('created_at','like','%'.$r->txtSearch.'%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->table . '.create', [
			'table' => $this->table,
			'title' => 'agregar solicitud',
			'users' => User::all(),
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqSolicitudes $request)
    {
        $solicitudes     = new solicitudes();
        $solicitudes->titular    = $request->titular;
        $solicitudes->correo    = $request->correo;
        $solicitudes->mensaje    = $request->mensaje;
        $solicitudes->estado_solicitud    = "No asignado";
        $solicitudes->user_id    = 1;

        $e = $solicitudes->save();

        return redirect()->route($this->table . '.index')->with(($e) ?
			'info' : 'danger', ($e) ?
			'Guardado con exito' : 'Ocurrio un problema al guardar la venta intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\solicitudes  $solicitudes
     * @return \Illuminate\Http\Response
     */
    public function show(solicitudes $solicitudes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\solicitudes  $solicitudes
     * @return \Illuminate\Http\Response
     */
    public function edit(solicitudes $solicitudes, $solicitude)
    {
        $data = $solicitudes::findOrFail($solicitude);
		return view($this->table . '.edit', [
			'table'        => $this->table,
			'title'        => 'Asignar solicitud',
			'data'         => $data,
			'users'       => User::all(),

		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\solicitudes  $solicitudes
     * @return \Illuminate\Http\Response
     */
    public function update(reqSolicitudesEdit $request, solicitudes $col, $solicitudes)
    {
        $data = $col::findOrFail($solicitudes);
        $data->titular        = $request->titular;
        $data->correo        = $request->correo;
        $data->mensaje        = $request->mensaje;
        $data->estado_solicitud = $request->estado_solicitud;
        $data->user_id        = $request->user_id;;
		$e                   = $data->save();
		return redirect()->route($this->table . '.index')->with(($e) ? 'info' : 'danger', ($e) ? 'Se edito un registro con exito. ' : 'Ocurrio un problema al editar el color intente de nuevo.'); 
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\solicitudes  $solicitudes
     * @return \Illuminate\Http\Response
     */
    public function destroy(solicitudes $solicitudes)
    {
        //
    }
}
