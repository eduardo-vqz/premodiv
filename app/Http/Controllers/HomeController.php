<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check()){
            $user = Auth::user();
            switch ($user->tipo_usuario) {
                case 700:
                    return redirect()->route('admin.index');
                break;
                case 56103:
                    return redirect()->route('cliente.home');
                break;
                default:
                    return redirect()->route('cliente.home');
                break;
            }
        } else return redirect()->route('cliente.home');
        //return view('home');
    }

    public function list(){
        return view('home');
    }
}
