<?php

namespace App\Http\Controllers;

use App\estado_carros;
use App\Http\Requests\reqEstadoCarros;
use App\Http\Requests\reqEstadoCarrosEdit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EstadoCarrosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = 'estado_carros';
    public function index()
    {
        $p = DB::table("estado_carros")
            ->select("estado_carros.*")
            ->paginate(10);
        return view($this->table . '.index', [
            'table' => $this->table,
            'title' => 'listado de estados',
            'data'  => $p
        ]);
    }

    public function search(Request $r)
    {
        if (!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para realizar la busqueda');

        return view($this->table . '.index', [
            'table' => $this->table,
            'title' => 'Listado de estados',
            'data'  => estado_carros::where('estado_carro', 'like', '%' . $r->txtSearch . '%')->paginate(),
            'txtSearch' => $r->txtSearch,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view($this->table . '.create', [
            'table' => $this->table,
            'title' => 'Agregar estado'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(reqEstadoCarros $request)
    {
        //
        $estado_carro = new estado_carros();
        $estado_carro->estado_carro = $request->estado_carros;
        $e = $estado_carro->save();
        return redirect()->route($this->table.'.index')->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar el estado carro intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\estado_carros  $estado_carros
     * @return \Illuminate\Http\Response
     */
    public function show(estado_carros $estado_carros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\estado_carros  $estado_carros
     * @return \Illuminate\Http\Response
     */
    public function edit(estado_carros $estado_carros, $estado_carro)
    {
        //
        $data = $estado_carros::findOrFail($estado_carro);
        return view($this->table.'.edit', [
            'table' =>  $this->table,
            'title'=>'Editar estado',
            'data'=> $data
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\estado_carros  $estado_carros
     * @return \Illuminate\Http\Response
     */
    public function update(reqEstadoCarrosEdit $request, estado_carros $col, $estado_carro)
    {
        $data = $col::findOrFail($estado_carro);
        $data->estado_carro = $request->estado_carros;
        $e = $data->save();
        return redirect()->route($this->table.'.index')->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el estado intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\estado_carros  $estado_carros
     * @return \Illuminate\Http\Response
     */
    public function destroy(estado_carros $estado_carros, $data)
    {
        $obj = $estado_carros::findOrFail($data);
        $m = $obj->delete();
        return redirect()->route($this->table.'.index')->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
