<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqSolicitudes extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "titular" => ['required','alpha'],
            "correo" => ['required','email:rfc,dns'],
            "mensaje" => ['required','regex:/^[-a-zA-Z0-9 .]+$/'],
        ];
    }
    public function messages()
    {
        return [
            'titular.required'  => 'Campo requerido.',
            'titular.alpha'  => 'Debe de ingresar solo letras.',
            'correo.required'  => 'Campo requerido.',
        ];
    }

    public function attributes()
    {
        return [
            'mensaje' => 'mensaje',
        ];
    }
}
