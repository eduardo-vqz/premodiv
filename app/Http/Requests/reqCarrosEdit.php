<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqCarrosEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "color_id" => ['required'],
            "modelo_id" => ['required'],
            "annio" => ['after:1990','before:2020'],
            "precio" => ['required','numeric'],
            "detalles" => ['required','string', 'max:100'],
            "foto_carro[]" => ['required'],
            "estado_carro_id" => ['required'],
        ];
    }
    public function messages()
    {
        return [
            'color_id.required'  => 'Seleccione un color.',
            'modelo_id.required'  => 'Seleccione un modelo.',
            'detalles.required'    => 'El :attribute es requerido.',
            'detalles.max'       => 'El :attribute no debe contener mas de 100 caracteres.',
            'foto_carro[].required'  => 'Seleccione una imagen.',
            'estado_carro_id.required'  => 'Seleccione un estado de carro.',
        ];
    }

    public function attributes()
    {
        return [
            'colores' => 'Color',
        ];
    }
}
