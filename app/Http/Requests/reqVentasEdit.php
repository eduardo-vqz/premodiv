<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqVentasEdit extends FormRequest
{
    /** 
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "numero_factura" => 'required|numeric',
            "cliente" => 'required|regex:/^[\pL\s\-]+$/u',
        ];
    }
    public function messages()
    {
        return [
            'numero_factura.required'  => 'El :attribute es requerido.',
            'numero_factura.numeric'  => 'El :attribute debe de ser numerico.',
            'cliente.required'    => 'El :attribute es requerido.',
        ];
    }

    public function attributes()
    {
        return [
            'numero_factura' => 'numero de factura',
            'cliente'   => 'cliente',
        ];
    }
}
