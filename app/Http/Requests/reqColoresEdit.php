<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqColoresEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "colores" => 'required|alpha|max:40',
        ];
    }
    public function messages()
    {
        return [
            'colores.required'  => 'El :attribute es requerido.',
            'colores.alpha'    => 'El :attribute debe ser de texto.',
            'colores.max'       => 'El :attribute no debe contener mas de 40 caracteres.',
        ];
    }

    public function attributes()
    {
        return [
            'colores' => 'Color',
        ];
    }
}
