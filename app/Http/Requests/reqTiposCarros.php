<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqTiposCarros extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            "tipos_carros" => 'required|alpha|max:40',
        ];
    }
    public function messages()
    {
        return [
            'tipos_carros.required'  => 'El :attribute es requerido.',
            'tipos_carros.alpha'    => 'El :attribute debe ser de texto.',
            'tipos_carros.max'       => 'El :attribute no debe contener mas de 40 caracteres.',
        ];
    }

    public function attributes()
    {
        return [
            'tipos_carros' => 'tipo carro',
        ];
    }
}
