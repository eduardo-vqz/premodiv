<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqSolicitudesEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "estado_solicitud" => ['required'],
            "user_id" => ['required'],
        ];
    }
    public function messages()
    {
        return [
            'estado_solicitud.required'  => 'Debe de seleccionar una opción distinta.',
        ];
    }

    public function attributes()
    {
        return [
            'mensaje' => 'mensaje',
        ];
    }
}
