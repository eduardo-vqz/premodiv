<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqModelosEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "modelo"         => 'required|alpha_dash|max:40',
            "marca_id"       => 'required',
            "tipo_carro_id"  => 'required',
        ];
    }
    public function messages()
    {
        return [
            'modelo.required'        => 'El :attribute es requerido.',
            'modelo.alpha_dash'      => 'El :attribute debe ser texto.',
            'modelo.max'             => 'El :attribute no debe contener mas de 40 caracteres.',
            'marca_id.required'      => 'Debe de seleccionar una marca',
            'tipo_carro_id.required' => 'Debe de seleccionar un tipo de carro',
        ];
    }

    public function attributes()
    {
        return [
            'modelo' => 'modelo',
        ];
    }
}
