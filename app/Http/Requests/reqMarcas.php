<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqMarcas extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "marcas" => 'required|alpha|max:40',
        ];
    }
    public function messages()
    {
        return [
            'marcas.required'  => ':attribute es requerido.',
            'marcas.alpha'     => ':attribute debe ser de texto.',
            'marcas.max'       => ':attribute no debe contener mas de 40 caracteres.',
        ];
    }

    public function attributes()
    {
        return [
            'marcas' => 'arca',
        ];
    }

}
