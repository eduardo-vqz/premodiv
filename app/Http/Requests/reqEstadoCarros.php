<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class reqEstadoCarros extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"estado_carros" => ['required', 'regex:/^[\pL\s\-]+$/u', 'max:100',],
		];
	}
	public function messages()
	{
		return [
			'estado_carros.required'  => 'El :attribute es requerido.',
			'estado_carros.max'       => 'El :attribute no debe contener mas de 100 caracteres.',
		];
	}

	public function attributes()
	{
		return [
			'estado_carros' => 'estado del carro',
		];
	}
}
