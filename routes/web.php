<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {
	//rutas de administracion

	//index de administracion
	Route::get('admin', 'AdminController@index')
		->name('admin.index')
		->middleware("permission:admin.home")
		->middleware("permission:user.home");

	//colores
	Route::resource('colores', 'ColoresController', ['except' => 'show'])->middleware("permission:admin.colores");
	Route::post('colores/search', 'ColoresController@search')->name('colores.search')->middleware("permission:admin.colores");

	//Rutas estado_carros
	Route::resource('estado_carros', 'EstadoCarrosController', ['except' => 'show'])->middleware("permission:admin.estado_carros");
	Route::post('estado_carros/search', 'EstadoCarrosController@search')->name('estado_carros.search')->middleware("permission:admin.estado_carros");

	//Rutas de modelos
	Route::resource('modelos', 'ModelosController', ['except' => 'show'])->middleware("permission:admin.modelos");
	Route::post('modelos/search', 'ModelosController@search')->name('modelos.search')->middleware("permission:admin.modelos");

	//Rutas de marcas
	Route::resource('marcas', 'MarcasController', ['except' => 'show'])->middleware("permission:admin.marcas");
	Route::post('marcas/search', 'MarcasController@search')->name('marcas.search')->middleware("permission:admin.marcas");

	//Rutas de tipos_carros
	Route::resource('tipos_carros', 'TiposCarrosController', ['except' => 'show'])->middleware("permission:admin.tipos_carros");
	Route::post('tipos_carros/search', 'TiposCarrosController@search')->name('tipos_carros.search')->middleware("permission:admin.tipos_carros");

	//Rutas de solicitudes
	Route::resource('solicitudes', 'SolicitudesController', ['except' => 'show'])->middleware("permission:admin.solicitudes");
	Route::post('solicitudes/search', 'SolicitudesController@search')->name('solicitudes.search')->middleware("permission:admin.solicitudes");

	//Rutas de carros
	Route::resource('carros', 'CarrosController', ['except' => 'show'])->middleware("permission:admin.carros");
	Route::post('carros/search', 'CarrosController@search')->name('carros.search')->middleware("permission:admin.carros");
	
	//Rutas de ventas
	Route::resource('ventas', 'VentasController', ['except' => 'show'])->middleware("permission:admin.ventas");
	Route::post('ventas/search', 'VentasController@search')->name('ventas.search')->middleware("permission:admin.ventas");

	Route::resource('detalle_ventas', 'DetalleVentasController', ['except' => 'show'])->middleware("permission:admin.detalle_ventas");
	Route::post('detalle_ventas/search', 'DetalleVentasController@search')->name('detalle_ventas.search')->middleware("permission:admin.detalle_ventas");
});
Route::resource('solicitudes', 'SolicitudesController', ['except' => 'show'])->middleware("permission:admin.solicitudes");
	Route::post('solicitudes/search', 'SolicitudesController@search')->name('solicitudes.search');