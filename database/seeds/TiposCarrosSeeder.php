<?php

use App\tipos_carros;
use Illuminate\Database\Seeder;

class TiposCarrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        tipos_carros::create([
            'tipo_carro' => 'Camioneta'
        ]);

        tipos_carros::create([
            'tipo_carro' => 'Sedan'
        ]);

        tipos_carros::create([
            'tipo_carro' => 'Pickup'
        ]);

        tipos_carros::create([
            'tipo_carro' => 'Deportivo'
        ]);

        tipos_carros::create([
            'tipo_carro' => 'Cupé'
        ]);
    }
}
