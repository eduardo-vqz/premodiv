<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(MarcasSeeder::class);
        $this->call(ColoresSeeder::class);
        $this->call(TiposCarrosSeeder::class);
        $this->call(ModelosSeeder::class);
        $this->call(RolUserSeeder::class);
    }
}
