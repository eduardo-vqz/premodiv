<?php

use App\marcas;
use Illuminate\Database\Seeder;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        marcas::create([
            'marca' => 'Toyota'
        ]);

        marcas::create([
            'marca' => 'Nissan'
        ]);

        marcas::create([
            'marca' => 'Jeep'
        ]);
        
        marcas::create([
            'marca' => 'Mazda'
        ]);
        
        marcas::create([
            'marca' => 'Honda'
        ]);

        marcas::create([
            'marca' => 'Ford'
        ]);

        marcas::create([
            'marca' => 'Chevrolet'
        ]);

        marcas::create([
            'marca' => 'Ferrari'
        ]);

        marcas::create([
            'marca' => 'Volkswagen'
        ]);

        marcas::create([
            'marca' => 'Mercedes-Benz'
        ]);

        marcas::create([
            'marca' => 'KIA'
        ]);

        marcas::create([
            'marca' => 'BMW'
        ]);

    }
}
