<?php

use App\estado_carros;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstadoCarrosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('estado_carros')->insert(
        	array('estado_carro' => 'Disponible')
        );
        DB::table('estado_carros')->insert(
        	array('estado_carro' => 'Vendido')
        );
    }
}
