<?php

use App\roles;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        roles::create(
            [
                'name'	=> 'root',//'Administrador'
                'slug'	=> 'admin',
                'description' => 'Tiene acceso a todo el sistema',
                'special'=> 'all-access'
            ]
        );
        roles::create( 
                [
                    'name'	=> 'user',
                    'slug'	=> 'user_96483',
                    'description' => 'Tiene acceso a todo el sistema',
                    
                ]
            );
    }
}
