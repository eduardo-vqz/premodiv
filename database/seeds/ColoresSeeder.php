<?php

use App\colores;
use Illuminate\Database\Seeder;

class ColoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        colores::create([
            'colores' => 'Blanco'
        ]);

        colores::create([
            'colores' => 'Negro'
        ]);

        colores::create([
            'colores' => 'Plata'
        ]);

        colores::create([
            'colores' => 'Gris'
        ]);

        colores::create([
            'colores' => 'Azul'
        ]);

        colores::create([
            'colores' => 'Rojo'
        ]);

        colores::create([
            'colores' => 'Beige'
        ]);

        colores::create([
            'colores' => 'Amarillo'
        ]);

        colores::create([
            'colores' => 'Verde'
        ]);

        colores::create([
            'colores' => 'Anaranjado'
        ]);

        colores::create([
            'colores' => 'Celeste'
        ]);

        colores::create([
            'colores' => 'Morado'
        ]);
        
    }
}
