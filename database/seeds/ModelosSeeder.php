<?php

use App\modelos;
use Illuminate\Database\Seeder;

class ModelosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Marca TOYOTA
        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 1,
            'modelo' => 'Aygo'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 1,
            'modelo' => 'Yaris'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 1,
            'modelo' => 'Corolla'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 1,
            'modelo' => 'RAV4'
        ]);
        
        //Marca NISSAN
        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 2,
            'modelo' => 'Micra'
        ]);

        modelos::create([
            'tipo_carro_id' => 3,
            'marca_id' => 2,
            'modelo' => 'NP300 Navara'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 2,
            'modelo' => 'Juke'
        ]);

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 2,
            'modelo' => '370Z'
        ]);
        
        //Marca JEEP
        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 3,
            'modelo' => 'Compass'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 3,
            'modelo' => 'Grand Cherokee'
        ]);

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 3,
            'modelo' => 'Wrangler'
        ]);

        modelos::create([
            'tipo_carro_id' => 5,
            'marca_id' => 3,
            'modelo' => 'Renegade'
        ]);

        //Marca MAZDA
        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 4,
            'modelo' => 'MX-5'
        ]);
        
        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 4,
            'modelo' => 'MX-30 2020'
        ]);

        modelos::create([
            'tipo_carro_id' => 3,
            'marca_id' => 4,
            'modelo' => 'B-2500'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 4,
            'modelo' => 'CX-3'
        ]);
        //Marca HONDA

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 5,
            'modelo' => 'Civic'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 5,
            'modelo' => 'CR-V'
        ]);
        
        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 5,
            'modelo' => 'Jazz'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 5,
            'modelo' => 'e'
        ]);

        //Marca FORD

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 6,
            'modelo' => 'Fiesta'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 6,
            'modelo' => 'EcoSport'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 6,
            'modelo' => 'Kuga 2020'
        ]);

        modelos::create([
            'tipo_carro_id' => 3,
            'marca_id' => 6,
            'modelo' => 'Ranger'
        ]);

        //Marca CHEVROLET
        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 7,
            'modelo' => 'Spark'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 7,
            'modelo' => 'Trax'
        ]);

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 7,
            'modelo' => 'Camaro'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 7,
            'modelo' => 'Orlando'
        ]);

        //Marca FERRARI
        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 8,
            'modelo' => 'GTC4Lusso'
        ]);

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 8,
            'modelo' => '812'
        ]);

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 8,
            'modelo' => 'Portofino'
        ]);

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 8,
            'modelo' => 'SF90'
        ]);

        //Marca VOLKSWAGEN

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 9,
            'modelo' => 'T-Roc'
        ]);

        modelos::create([
            'tipo_carro_id' => 3,
            'marca_id' => 9,
            'modelo' => 'Amarok'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 9,
            'modelo' => 'Touareg'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 9,
            'modelo' => 'Polo'
        ]);

        //Marca MERCEDES-BENZ

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 10,
            'modelo' => 'GLA'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 10,
            'modelo' => 'GLB'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 10,
            'modelo' => 'Clase A'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 10,
            'modelo' => 'CLA 2019'
        ]);

        //Marca KIA
        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 11,
            'modelo' => 'Picanto'
        ]);
        
        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 11,
            'modelo' => 'ceed'
        ]);
        
        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 11,
            'modelo' => 'Stonic'
        ]);

        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 11,
            'modelo' => 'Sportage'
        ]);

        //Marca BMW
        modelos::create([
            'tipo_carro_id' => 1,
            'marca_id' => 12,
            'modelo' => 'X4'
        ]);

        modelos::create([
            'tipo_carro_id' => 4,
            'marca_id' => 12,
            'modelo' => 'i8'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 12,
            'modelo' => 'Serie 1 2020'
        ]);

        modelos::create([
            'tipo_carro_id' => 2,
            'marca_id' => 12,
            'modelo' => 'Serie 1 2017'
        ]);
    }
}
