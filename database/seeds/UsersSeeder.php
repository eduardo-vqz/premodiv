<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        user::create([
            'name' => 'admin',
            'last_name' => 'admin',
            'usuario' => 'admin_admin',
            'email' => 'admin@gmail.com',
            'phone' => '78886666',
            'tipo_usuario' => '700',
            'password' => Hash::make('Admin_adm1n')
        ]);

        user::create([
            'name' => 'user',
            'last_name' => 'user',
            'usuario' => 'user',
            'email' => 'user@gmail.com',
            'phone' => '78412236',
            'tipo_usuario' => '56103',
            'password' => Hash::make('user123')
        ]);
    }
}
 