<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFotosCarrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotos_carros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('carro_id')->unsigned();
            $table->foreign('carro_id')->references('id')->on('carros');
            $table->string('foto_carro');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fotos_carros');
    }
}
