@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
		<div class="row">
			<div class="col-auto">
				<h5 class="card-title text-uppercase font-weight-bold">
					<i class="fas fa-align-justify"></i>
					Listado de estado de vehículos
				</h5>
			</div>
	  	</div>	
	
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>







<table class="table table-borderless table-hover table-responsive-lg">
	<thead>
		<tr class="text-uppercase font-italic">
			<th scope="col">Estados</th>
			@can($table.'.update')
				<th scope="col">Editar</th>
			@endcan
			@can($table.'.update')
				<th scope="col">Eliminar</th>
			@endcan
		</tr>
	</thead>
	<tbody>
		@foreach ($data as $e)
		<tr>
			<th scope="row">{{ $e->estado_carro }}</th>
			@can($table.'.update')
				<td>
					<a href="{{ route($table.'.edit', ['estado_carro' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
						<i class="fas fa-pen"></i>
					</a>
				</td>
			@endcan
			@can($table.'.update')
				<td>
					<form action="{{ route($table.'.destroy', ['estado_carro' => $e->id ]) }}" method="post"
						class="frmDelete">
						@csrf
						@method('DELETE')
						<button class="btn red-text btnDelete" type="button" tag="{{ $e->estado_carro }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
							<i class="fas fa-eraser"></i>
						</button>
					</form>
				</td>
			@endcan
		</tr>
		@endforeach
	</tbody>
</table>
{{ $data->render() }}
@endsection