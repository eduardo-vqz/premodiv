@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
		<div class="row">
			<div class="col-auto">
				<h5 class="card-title text-uppercase font-weight-bold">
					<i class="fas fa-align-justify"></i>
					Listado de colores
				</h5>
			</div>
	  </div>	
    <table class="table table-borderless table-hover table-responsive-lg">
    <thead>
      <tr class="text-uppercase font-italic">
          <th scope="col">Colores</th>
          @can($table.'.update')
            <th scope="col">Editar</th>
          @endcan
          @can($table.'.update')
            <th scope="col">Eliminar</th>
          @endcan
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
      <tr>
        <th scope="row">{{ $e->colores }}</th>
        @can($table.'.update')
          <th>
            <a href="{{ route($table.'.edit', ['colore' => $e->id ]) }}"  class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
              <i class="fas fa-pen"></i>
            </a>
          </th>
        @endcan
        @can($table.'.update')
            <th>
              <form action="{{ route($table.'.destroy', ['colore' => $e->id ]) }}" method="post" class="frmDelete">
                @csrf
                @method('DELETE')
                <button class="btn red-text btnDelete" type="button" tag="{{ $e->id }}"  data-toggle="tooltip" data-placement="right" title="Eliminar registro">
                  <i class="fas fa-eraser"></i>
                </button>
              </form>
            </th>
        @endcan
      </tr>
      @endforeach
    </tbody>
  </table>  
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
</div>
@endsection