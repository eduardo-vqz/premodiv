@extends('layouts.list')
@section('list')

<div class="card" style="width: auto;">
	<div class="card-body">
		<div class="row">
			<div class="col-auto">
				<h5 class="card-title text-uppercase font-weight-bold">
					<i class="fas fa-align-justify"></i>
					Listado de ventas
				</h5>
			</div> 
	  </div>	
    <table class="table table-borderless table-hover table-responsive-lg">
    <thead>
      <tr class="text-uppercase font-italic">
          <th scope="col">Vendedor</th>
          <th scope="col">No Factura</th>
          <th scope="col">Cliente</th>
          <th scope="col">Fecha</th>
          <th scope="col">Seguimiento</th>
          @can($table.'.update')
            <th scope="col">Editar</th>
          @endcan
          @can($table.'.update')
            <th scope="col">Eliminar</th>
          @endcan
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
        <tr>
          <th scope="row">{{ $e->name }}</th>
          <td>{{ $e->numero_factura }}</td>
          <td>{{ $e->cliente }}</td>
          <td>{{ $e->created_at }}</td>
          @can('detalle_ventas.create')
              <td>
                <a href="{{ route('detalle_ventas.create', ['venta' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Realizar proceso de venta">
                  <i class="fas fa-cash-register"></i>
                </a>
              </td>
            @endcan
            @can($table.'.update')
              <td>
                <a href="{{ route($table.'.edit', ['venta' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
                  <i class="fas fa-pen"></i>
                </a>
              </td>
            @endcan
            @can($table.'.update')
                <td>
                  <form action="{{ route($table.'.destroy', ['venta' => $e->id ]) }}" method="post" class="frmDelete">
                    @csrf
                    @method('DELETE')
                    <button class="btn red-text btnDelete" type="button" tag="{{ $e->id }}"  data-toggle="tooltip" data-placement="right" title="Eliminar registro">
                        <i class="fas fa-eraser"></i>
                    </button>
                  </form>
                </td>
            @endcan
        </tr>
      @endforeach
    </tbody>
  </table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
</div>
@endsection