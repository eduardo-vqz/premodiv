@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
		<div class="row">
			<div class="col-auto">
				<h5 class="card-title text-uppercase font-weight-bold">
					<i class="fas fa-align-justify"></i>
					Listado de vehículos
				</h5>
			</div>
	  	</div>	
		  <table class="table table-borderless table-hover table-responsive-lg">
			<thead>
				<tr class="text-uppercase font-italic">
					<th scope="col">Marca</th>
					<th scope="col">Color</th>
					<th scope="col">Modelo</th>
					<th scope="col">Año</th>
					<th scope="col">Detalles</th>
					<th scope="col">Estado</th>
					<th scope="col">Precio</th>
					@can($table.'.update')
						<th scope="col">Editar</th>
					@endcan
					@can($table.'.update')
						<th scope="col">Eliminar</th>
					@endcan
				</tr>
			</thead> 
			<tbody>
				@foreach ($data as $e)
				
				<tr>
				<th scope="row">  {{ $e->marca }} </th>
				
					<td>{{ $e->colores}}</td>
					<td>{{ $e->modelo }}</td>
					<td>{{ $e->annio }}</td>
					<td>{{ $e->detalles }}</td>
					<td>{{ $e->estado_carro }}</td>
					<td>$ {{ $e->precio }}</td>
					@can($table.'.update')
					<td>
						<a href="{{ route($table.'.edit', ['carro' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
							<i class="fas fa-pen"></i>
						</a>
					</td>
					@endcan
					@can($table.'.update')
					<td>
						<form action="{{ route($table.'.destroy', ['carro' => $e->id ]) }}" method="post" class="frmDelete">
							@csrf
							@method('DELETE')
							<button class="btn red-text btnDelete" type="button" tag="{{ $e->id}}"  data-toggle="tooltip" data-placement="right" title="Eliminar registro">
								<i class="fas fa-eraser"></i>
							</button>
						</form>
					</td>
					@endcan
				</tr>

				

				@endforeach
			</tbody>
		</table>
		<div class="">
			{{ $data->render() }}
		</div>
	</div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">

		... {{-- <img src="{{ asset('css/app.css'.img->foto_carro) }}" alt=""> --}}
		
		</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary">Save changes</button>
		</div>
	</div>
	</div>
</div>
@endsection

