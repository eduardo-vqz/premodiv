@extends('layouts.form')
@section('form')
<style>
	.centrar-letras{
		margin: auto;
	}
</style>
<br>
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
		<div class="card-header">
			<div class="row">
				<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
					Nuevo vehículo
				</div>
				<div class="col-2">
					<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle="tooltip" data-placement="right" title="Cerrar formulario">
						<i class="far fa-times-circle fa-2x"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div id="formAddcarros">
				<form action="{{ route($table.'.store') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
					@csrf
					<div class="form-row justify-content-around">
						<div class="col-md-6 mb-3">
							<label for="color_id">Color</label>
							<select class="custom-select" id="color_id" name="color_id">
								<option selected disabled value="">Seleccione un color</option>
								@foreach ($colores as $color)
								<option value="{{$color->id}}">
									{{$color->colores}}
								</option>
								@endforeach
							</select>
							@error('color_id')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
						<div class="col-md-6 mb-3">
							<label for="modelo_id">Modelo</label>
							<select class="custom-select" id="modelo_id" name="modelo_id">
								<option selected disabled value="">Seleccione un modelo</option>
								@foreach ($modelos_marcas as $marca_modelo)
								<option value="{{$marca_modelo->id}}">
									{{$marca_modelo->marca ." - ". $marca_modelo->modelo}}
								</option>
								@endforeach
							</select>
							@error('modelo_id')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
						<div class="col-md-6 mb-3">
							<label for="annio">Año</label>
							<input type="text" class="form-control" id="annio" value="{{ old('annio') }}" name="annio">
							@error('annio')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
						<div class="col-md-6 mb-3">
							<label for="precio">Precio</label>
							<input type="text" class="form-control" id="precio" value="{{ old('precio') }}" name="precio">
							@error('precio')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
						<div class="col-md-12 mb-3">
							<label for="detalles">Detalles</label>
							<textarea type="text" cols="30" rows="2" class="form-control" id="detalles" value="{{ old('detalles') }}" name="detalles"></textarea>
							@error('detalles')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
						<div class="col-md-12 mb-3">
							<label for="detalles">Fotos del vehiculo</label>

							<input type="file" name="foto_carro[]" id="foto_carro" accept=".jpg,.jpeg,.png" class="form-control" multiple ondrop="dropHandler(event);" ondragover="dragOverHandler(event);">
							
						</div>
						<div class="col-md-6 mb-3">
							<label for="estado_carro_id">Estado</label>
							<select class="custom-select" id="estado_carro_id" name="estado_carro_id">
								<option selected disabled value="">Seleccione un estado</option>
								@foreach ($estado_carros as $estado_carro)
								<option value="{{$estado_carro->id}}">
									{{$estado_carro->estado_carro}}
								</option>
								@endforeach
							</select>
							@error('estado_carro_id')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
					</div>
					<!-- Cambiar hasta aqui lo demas es igual -->
					<div class="col s12 push-s8">
						
					</div>
			</div>
		</div>
		<div class="card-footer text-muted">
			<button class="btn btn-primary" type="submit">Guardar</button>
		</div>
	</form>
		</div>
	</div>
</div>
<br>
@endsection
<script>
	$('.file-upload').file_upload();
</script>

<style>
	#foto_carro {
	height: auto;
}
</style>