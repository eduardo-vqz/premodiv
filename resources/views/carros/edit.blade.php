@extends('layouts.form')
@section('form')
<style>
	.centrar-letras {
		margin: auto;
	}
</style>
<br>
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
			<div class="card-header">
				<div class="row">
					<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
						Actualizar vehículo
					</div>
					<div class="col-2">
						<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle=" tooltip"
							data-placement="right" title="Cerrar formulario">
							<i class="far fa-times-circle fa-2x"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div id="formAddcarros">
					<form action="{{ route($table.'.update', ['carro' => $data->id]) }}" method="POST"
						accept-charset="UTF-8" enctype="multipart/form-data">
						@csrf
						@method('PUT')
						<div class="form-row justify-content-around">
							<div class="col-md-6 mb-3">
								<label for="color_id">Color</label>
								<select class="custom-select" id="color_id" name="color_id" required>
									<option selected disabled value="">Seleccione un color</option>
									@foreach ($colores as $color)
									@if ($data->color_id == $color->id)
									<option value="{{$color->id}}" selected>
										{{$color->colores}}
									</option>
									@else
									<option value="{{$color->id}}">
										{{$color->colores}}
									</option>
									@endif
									@endforeach
								</select>
								@error('color_id')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3">
								<label for="modelo_id">Modelo</label>
								<select class="custom-select" id="modelo_id" name="modelo_id" required>
									<option selected disabled value="">Seleccione un modelo</option>
									@foreach ($modelos_marcas as $marca_modelo)
									@if ($marca_modelo->id == $data->modelo_id)
									<option value="{{$marca_modelo->id}}" selected>
										{{$marca_modelo->marca ." - ". $marca_modelo->modelo}}
									</option>
									@else
									<option value="{{$marca_modelo->id}}">
										{{$marca_modelo->marca ." - ". $marca_modelo->modelo}}
									</option>
									@endif
									@endforeach
								</select>
								@error('modelo_id')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3">
								<label for="annio">Año</label>
								<input type="text" class="form-control" id="annio" value="{{ $data->annio }}"
									name="annio" required>
									@error('annio')
										<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
											role="alert">
											<strong>{{ $message }}</strong>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									@enderror
							</div>
							<div class="col-md-6 mb-3">
								<label for="precio">Precio</label>
								<input type="text" class="form-control" id="precio" value="{{ $data->precio }}"
									name="precio" required>
									@error('precio')
										<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
											role="alert">
											<strong>{{ $message }}</strong>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									@enderror
							</div>
							<div class="col-md-12 mb-3">
								<label for="detalles">Detalles</label>
								<textarea type="text" cols="30" rows="10" class="form-control" id="detalles"
									name="detalles" required>
								{{ trim($data->detalles) }}
							</textarea>
							@error('detalles')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
							</div>
							<div class="col-md-12 mb-3">
								<label for="detalles">Fotos del vehiculo</label>

								<input type="file" name="foto_carro[]" id="foto_carro" accept=".jpg,.jpeg,.png"
									class="form-control" multiple ondrop="dropHandler(event);"
									ondragover="dragOverHandler(event);">
									@error('foto_carro[]')
										<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
											role="alert">
											<strong>{{ $message }}</strong>
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									@enderror
							</div>
							<div class="col-md-6 mb-3">
								<label for="estado_carro_id">Estado</label>
								<select class="custom-select" id="estado_carro_id" name="estado_carro_id" required>
									<option selected disabled value="">Seleccione un estado</option>
									@foreach ($estado_carros as $estado_carro)
									@if ($estado_carro->id == $data->estado_carro_id)
									<option value="{{$estado_carro->id}}" selected>
										{{$estado_carro->estado_carro}}
									</option>
									@else
									<option value="{{$estado_carro->id}}">
										{{$estado_carro->estado_carro}}
									</option>
									@endif
									@endforeach
								</select>
								@error('estado_carro_id')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
						</div>
						<!-- Cambiar hasta aqui lo demas es igual -->
						<div class="col s12 push-s8">

						</div>
				</div>
			</div>
			<div class="card-footer text-muted">
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
			</form>
		</div>
	</div>
</div>
<br>
@endsection

<script>
	$('.file-upload').file_upload();
</script>

<style>
	#foto_carro {
		height: auto;
	}
</style>