@extends('layouts.form')
@section('form')
<style>
	.centrar-letras {
		margin: auto;
	}
</style>
<br>
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
			<div class="card-header">
				<div class="row">
					<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
						Nueva solicitud
					</div>
					<div class="col-2">
						<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle=" tooltip"
							data-placement="right" title="Cerrar formulario">
							<i class="far fa-times-circle fa-2x"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div id="formAddSolicitud">
					<form action="{{ route($table.'.store') }}" method="POST" accept-charset="UTF-8"
						enctype="multipart/form-data">
						@csrf
						<div class="form-row">
							<div class="col-md-12 mb-3">
								<label for="validationDefault01">Titular</label>
								<input type="text" class="form-control" id="titular" value="{{ old('titular') }}"
									name="titular">
								@error('titular')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
							<div class="col-md-12 mb-3">
								<label for="validationDefault01">Correo</label>
								<input type="text" class="form-control" id="correo" value="{{ old('correo') }}"
									name="correo">
								@error('correo')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
							<div class="col-md-12 mb-3">
								<label for="mensaje">Mensaje</label>
								<textarea type="text" cols="30" rows="2" class="form-control" id="mensaje" value="{{ old('mensaje') }}" name="mensaje"></textarea>
								@error('mensaje')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
						</div>
						<!-- Cambiar hasta aqui lo demas es igual -->
						<div class="col s12 push-s8">

						</div>
				</div>
			</div>
			<div class="card-footer text-muted">
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
			</form>
		</div>
	</div>
</div>
<br>
@endsection