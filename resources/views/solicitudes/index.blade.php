@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
		<div class="row">
			<div class="col-auto">
				<h5 class="card-title text-uppercase font-weight-bold">
					<i class="fas fa-align-justify"></i>
					solicitudes
				</h5>
			</div>
	  	</div>	 
	<table class="table table-borderless table-hover table-responsive-lg">
	<thead>
		<tr class="text-uppercase font-italic">
			<th scope="col">Titular</th>
			<th scope="col">Correo</th>
      <th scope="col">Mensaje</th>
      <th scope="col">E/S</th>
			<th scope="col">Usuario</th>
			<th scope="col">Fecha</th>
			@can($table.'.update')
				<th scope="col">Asignar</th>
			@endcan
			@can($table.'.update')
				<th scope="col">Eliminar</th>
			@endcan
		</tr>
	</thead>
	<tbody>
		@foreach ($data as $e)
			<tr>
				<th scope="row">{{ $e->titular }}</th>
        <td>{{ $e->correo }}</td>
        <td>{{ $e->mensaje }}</td>
        <td>{{ $e->estado_solicitud }}</td>
        <td>{{ $e->name }}</td>
        <td>{{ $e->created_at }}</td>
				@can($table.'.update')
					<td>
						<a href="{{ route($table.'.edit', ['solicitude' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
							<i class="fas fa-address-book"></i>
						</a>
					</td>
				@endcan
				@can($table.'.update')
					<td>
						<form action="{{ route($table.'.destroy', ['solicitude' => $e->id ]) }}" method="post"
						class="frmDelete">
							@csrf
							@method('DELETE')
							<button class="btn red-text btnDelete" type="button" tag="{{ $e->titular }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
								<i class="fas fa-eraser"></i>
							</button>
						</form>
					</td>
				@endcan
			</tr>
		@endforeach
	</tbody>
	</table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>
@endsection