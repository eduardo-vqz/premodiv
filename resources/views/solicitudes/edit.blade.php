@extends('layouts.form')
@section('form')
<style>
	.centrar-letras{
		margin: auto;
	}
</style>
<br>
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
		<div class="card-header">
			<div class="row">
				<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
					Asignar solicitud
				</div>
				<div class="col-2">
					<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle="tooltip" data-placement="right" title="Cerrar formulario">
						<i class="far fa-times-circle fa-2x"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div id="formUpdateestadocarro">
				<form action="{{ route($table.'.update', ['solicitude' => $data->id]) }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
					@csrf
					@method('PUT')
					<div class="form-row">
							<div class="col-md-12 mb-3">
								<label for="validationDefault01">Titular</label>
								<input type="text" class="form-control" id="titular" value="{{ $data->titular }}"
									name="titular" readonly>
								@error('titular')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
							<div class="col-md-12 mb-3">
								<label for="validationDefault01">Correo</label>
								<input type="text" class="form-control" id="correo" value="{{ $data->correo  }}"
									name="correo" readonly>
								@error('correo')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
							<div class="col-md-12 mb-3">
								<label for="mensaje">Mensaje</label>
								<textarea type="text" cols="30" rows="2" class="form-control" id="mensaje" name="mensaje" readonly>
									{{ trim($data->mensaje) }}
								</textarea>
								@error('mensaje')
									<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
										role="alert">
										<strong>{{ $message }}</strong>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3">
								<label for="validationDefault04">Estado</label>
								<select class="custom-select" id="estado_solicitud" name="estado_solicitud">
									<option selected disabled value="{{ $data -> estado_solicitud}}">{{ $data-> estado_solicitud}}</option>
									<option value="No asignado">No asignado</option>
									<option value="Asignado">Asignado</option>
									<option value="Flinlizado">Finalizado</option>
								</select>
								@error('estado_solicitud')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3">
							<label for="user_id">Asignar usuario</label>
							<select class="custom-select" id="user_id" name="user_id">
								<option selected disabled value="">Seleccione un usuario</option>
								@foreach ($users as $user)
									@if ($user->id == $data->user_id)
										<option value="{{$user->id}}" selected>
											{{$user->name}}
										</option>
									@else
										<option value="{{$user->id}}">
											{{$user->name}}
										</option>
									@endif
								@endforeach
							</select>
							@error('user_id')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
						</div>
						<!-- Cambiar hasta aqui lo demas es igual -->
						<div class="col s12 push-s8">

						</div>
				</div>
			</div>
			<div class="card-footer text-muted">
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
	</form>
		</div>
	</div>
</div>
<br>
@endsection