@extends('layouts.form')
@section('form')
<!-- En el atributo: action solo cambiar la palabra  store / update  
  si es creacion se usa store, si es edicion update
  Tambien cambiar el id del formulario y de cada campo para que funcione materialize. 
-->
<style>
	.centrar-letras {
		margin: auto;
	}
</style>
<br>
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
			<div class="card-header">
				<div class="row">
					<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
						Nuevo modelo
					</div>
					<div class="col-2">
						<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle=" tooltip"
							data-placement="right" title="Cerrar formulario">
							<i class="far fa-times-circle fa-2x"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div id="formAddModelos">
					<form action="{{ route($table.'.store') }}" method="POST" accept-charset="UTF-8"
						enctype="multipart/form-data">
						@csrf
						<div class="form-row">
							<div class="col-md-12 mb-3">
								<label for="validationDefault01">Modelo</label>
								<input type="text" class="form-control" id="modelo" value="{{ old('modelo') }}"
									name="modelo">
								@error('modelo')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3">
								<label for="validationDefault04">Marca</label>
								<select class="custom-select" id="marca_id" name="marca_id">
									<option selected disabled value="">Seleccione una marca</option>
									@foreach ($marcas as $marca)
									<option value="{{$marca->id}}">{{$marca->marca}}</option>
									@endforeach
								</select>
								@error('marca_id')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3">
								<label for="validationDefault04">Tipo de auto</label>
								<select class="custom-select" id="tipo_carro_id" name="tipo_carro_id">
									<option selected disabled value="">Seleccione un tipo</option>
									@foreach ($tipos_carros as $tipo_carro)
									<option value="{{$tipo_carro->id}}">{{$tipo_carro->tipo_carro}}</option>
									@endforeach
								</select>
								@error('tipo_carro_id')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								@enderror
							</div>
						</div>
						<!-- Cambiar hasta aqui lo demas es igual -->
						<div class="col s12 push-s8">

						</div>
				</div>
			</div>
			<div class="card-footer text-muted">
				<button class="btn btn-primary" type="submit">Guardar</button>
			</div>
			</form>
		</div>
	</div>
</div>
<br>
@endsection