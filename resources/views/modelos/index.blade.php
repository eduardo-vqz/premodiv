@extends('layouts.list')
@section('list')
<div class="card" style="width: auto;">
	<div class="card-body">
		<div class="row">
			<div class="col-auto">
				<h5 class="card-title text-uppercase font-weight-bold">
					<i class="fas fa-align-justify"></i>
					Listado de modelos
				</h5>
			</div>
	  	</div>	
	<table class="table table-borderless table-hover table-responsive-lg">
	<thead>
		<tr class="text-uppercase font-italic">
			<th scope="col">Modelos</th>
			<th scope="col">Marca</th>
			<th scope="col">Tipo de carro</th>
			@can($table.'.update')
				<th scope="col">Editar</th>
			@endcan
			@can($table.'.update')
				<th scope="col">Eliminar</th>
			@endcan
		</tr>
	</thead>
	<tbody>
		@foreach ($data as $e)
			<tr>
				<th scope="row">{{ $e->modelo }}</th>
				<td>{{ $e->marca }}</td>
				<td>{{ $e->tipo_carro }}</td>
				@can($table.'.update')
					<td>
						<a href="{{ route($table.'.edit', ['modelo' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
							<i class="fas fa-pen"></i>
						</a>
					</td>
				@endcan
				@can($table.'.update')
					<td>
						<form action="{{ route($table.'.destroy', ['modelo' => $e->id ]) }}" method="post"
						class="frmDelete">
							@csrf
							@method('DELETE')
							<button class="btn red-text btnDelete" type="button" tag="{{ $e->modelo }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
								<i class="fas fa-eraser"></i>
							</button>
						</form>
					</td>
				@endcan
			</tr>
		@endforeach
	</tbody>
	</table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>
@endsection