@extends('layouts.form')
@section('form')
<style>
	.centrar-letras{
		margin: auto;
	}
</style>
<br>
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
		<div class="card-header">
			<div class="row">
				<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
					Detalles de venta a realizar
				</div>
				<div class="col-2">
					<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle="tooltip" data-placement="right" title="Cerrar formulario">
						<i class="far fa-times-circle fa-2x"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div id="formAddventas">
				<form action="{{ route($table.'.store') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
					
	</form>
		</div>
	</div>
</div>
<br>
@endsection