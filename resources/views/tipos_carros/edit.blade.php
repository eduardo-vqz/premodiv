@extends('layouts.form')
@section('form')
<style>
	.centrar-letras{
		margin: auto;
	}
</style>
<br>
<div class="row justify-content-around">
	<div class="col-xs-12 col-md-6">
		<div class="card text-center">
		<div class="card-header">
			<div class="row">
				<div class="col-10 text-uppercase font-weight-bold font-italic centrar-letras">
					Actualizar tipo de vehículo
				</div>
				<div class="col-2">
					<a href="{{ URL::previous() }}" class="btn red darken-1 data-toggle="tooltip" data-placement="right" title="Cerrar formulario">
						<i class="far fa-times-circle fa-2x"></i>
					</a>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div id="formAddtipos_carros">
				<form action="{{ route($table.'.store') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
					@csrf
					<div class="form-row justify-content-around">
						<div class="col-md-8 mb-3">
							<label for="tipos_carros">Tipo de auto</label>
							<input type="text" class="form-control" id="tipos_carros" value="{{ $data->tipo_carro }}" name="tipos_carros">
							@error('tipos_carros')
								<div class="alert alert-warning alert-dismissible fade show" style="margin-top: 5px;"
									role="alert">
									<strong>{{ $message }}</strong>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							@enderror
						</div>
					</div>
					<!-- Cambiar hasta aqui lo demas es igual -->
					<div class="col s12 push-s8">
						
					</div>
			</div>
		</div>
		<div class="card-footer text-muted">
			<button class="btn btn-primary" type="submit">Guardar</button>
		</div>
	</form>
		</div>
	</div>
</div>
<br>
@endsection

