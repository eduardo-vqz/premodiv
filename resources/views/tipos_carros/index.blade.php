@extends('layouts.list')
@section('list')

<div class="card" style="width: auto;">
	<div class="card-body">
		<div class="row">
			<div class="col-auto">
				<h5 class="card-title text-uppercase font-weight-bold">
					<i class="fas fa-align-justify"></i>
					Listado de tipos de vehículos
				</h5>
			</div>
	  </div>	
    <table class="table table-borderless table-hover table-responsive-lg">
    <thead class="text-uppercase font-italic">
      <tr>
          <th scope="col">Tipos</th>
          @can($table.'.update')
            <th scope="col">Editar</th>
          @endcan
          @can($table.'.update')
            <th scope="col">Eliminar</th>
          @endcan
      </tr>
    </thead>
    <tbody>
      @foreach ($data as $e)
      <tr>
        <th scope="row">{{ $e->tipo_carro }}</th>
        @can($table.'.update')
          <th>
            <a href="{{ route($table.'.edit', ['tipos_carro' => $e->id ]) }}" class="btn" data-toggle="tooltip" data-placement="right" title="Actualizar registro">
              <i class="fas fa-pen"></i>
            </a>
          </th>
        @endcan
        @can($table.'.update')
            <th>
              <form action="{{ route($table.'.destroy', ['tipos_carro' => $e->id ]) }}" method="post" class="frmDelete">
                @csrf
                @method('DELETE')
                <button class="btn red-text btnDelete" type="button" tag="{{ $e->tipo_carro }}" data-toggle="tooltip" data-placement="right" title="Eliminar registro">
                  <i class="fas fa-eraser"></i>
                </button>
              </form>
            </th>
        @endcan
      </tr>
      @endforeach
    </tbody>
  </table>
	<div class="">
		{{ $data->render() }}
	</div>
	</div>
	</div>





@endsection