@extends('layouts.app')
@section('content')
	<div class="container-fluid">
		<div class="row justify-content-around">
			<div class="col-auto">
				<form class="form-inline position-relative d-inline-block my-2" action="{{ route($table.'.search') }}" method="POST">
					@csrf
					@isset($txtSearch)
						<a href="{{ route($table.'.index') }}" class="waves-effect waves-light btn-flat left prefix">
							<i class="fas fa-times" style="color: #02368E;"></i>
						</a>
					@endisset
					<input class="form-control" type="text" class="validate" required name="txtSearch" id="search" value="{{ (isset($txtSearch))? $txtSearch :'' }}" placeholder="Buscar" aria-label="Buscar">
					<button class="btn position-absolute btn-search" type="submit" data-toggle="tooltip" data-placement="right" title="Buscar">
						<i class="fas fa-search"></i>
					</button>
				</form>
				@can($table.'.create')
					<a href="{{ route($table.'.create') }}" class="btn btn-primary" data-toggle="togle" data-placement="right" title="Agregar nuevo registro">
						<i class="fas fa-plus fa-1x" style="color:#E5EFFF"></i> Nuevo
					</a>
				@endcan
			</div>

			<div class="col-md-12 col-lg-12">
				@yield('list')
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="{{ asset('js/jq.js')}}"></script>
	<script>
		$(document).ready(function () {
			$('.btnAlertRemove').click(function (e) {
				$('.contentAlert').hide('fast');
			 });
			$(".btnDelete").click(function (e) {
				e.preventDefault();
				console.log('Click')
				var tag 		= $(this).attr('tag');
				var frmDelete 	= $(this).parent('.frmDelete');
				if(confirm("Esta seguro de borrar este registro: "+tag))
					return frmDelete.submit();
				return false;
			});
		});
	</script>
	@yield('scripts-list')
@endsection
